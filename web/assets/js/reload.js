 /* global firebase */

var uidUserSelected="";
 var userNameSelected;
 var arrayUsers = [];
 var establishment;
var arrayUsersVetoed = [];
 $(document).ready(function() {
 var config = {
            apiKey: "AIzaSyD7-bVTa9iV2NNrlFqWqUMk-VQx9H4LnFs",
            authDomain: "dispositivo1-202101.firebaseapp.com",
            databaseURL: "https://dispositivo1-202101.firebaseio.com",
            projectId: "dispositivo1-202101",
            storageBucket: "dispositivo1-202101.appspot.com",
            messagingSenderId: "629911785392"
          };
    firebase.initializeApp(config);
    iniciarUsuarios($("#idEstablishment").val());
    establishment = $("#idEstablishment").val();
});
$( "#btnReload" ).click(function() {

    var credits = $("#selectCredits").val();
    if (uidUserSelected !== "")
        recargar(credits);
    else
        alert("Seleccione un usuario");
}); 
function recargar(creditos){
    var creditsObj = new Object();
    creditsObj.credits = creditos;
    var starCountRef = firebase.database().ref('credits/' + uidUserSelected ).set(creditsObj)
    .then(function(result) {
        alert("Recarga Exitosa");
    }).catch(function (error) {
        alert("Error Recargando ");
    });
    var myLoadHistory = new Object();
        var f = new Date();
        var date = f.getDate() + "/" + (f.getMonth() +1) + "/" + f.getFullYear();
        myLoadHistory.creditsLoads = creditos;
        myLoadHistory.dateLoads = date;
        var key = Math.floor(Math.random() * (100000000 - 1) + 0);
    
            firebase.database().ref('history/users/' + uidUserSelected + "/load/"+key).set(myLoadHistory)
            .then(function(result) {
               console.log("Exito");
            }).catch(function (error) {
               console.log("Error");
            });
    
}
function iniciarUsuarios(establishmentId){
    var starCountRef = firebase.database().ref('session/establishment/' + establishmentId + '/users/');
    starCountRef.on('value', function(snapshot) {
           var songs = JSON.stringify(snapshot);
           var obj = JSON.parse(songs);
           var numUsers=0;
           arrayUsers = [];
           for(let i in obj){
               var user = new Object();
               user.sessionDateStart = obj[i].sessionDateStart;
               user.sessionState = obj[i].sessionState;
               user.sessionUserId = obj[i].sessionUserId;
               user.sessionUserToken = obj[i].sessionUserToken;
               user.sessionUserName = obj[i].sessionUserName;
               user.sessionUserImage = obj[i].sessionUserImage;
               if( user.sessionState === "active" & user.sessionUserName!=="Admin")
                   arrayUsers.push(user);
               else if(user.sessionState === "vetoed")
                   arrayUsersVetoed.push(user);
               
           }
           console.log(arrayUsers);
           console.log(arrayUsersVetoed);
           var json = JSON.stringify(arrayUsers);
			$.post('servletItem', {
				json:json,
                                option:6
			}, function(responseText) {
				$("#columnsUsers").html(responseText);
			});
      });
}
function selectUser(uidUser,userName){
    uidUserSelected = uidUser;
    userNameSelected = userName;
    
    $("#userSelected").html("Usuario seleccionado: "+userNameSelected+"<br/> UID:"+uidUserSelected);
} 
function unVetoedUser(){
     var opciones="toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=yes,width=420,height=500,top=85,left=140";
    window.open("/JukeboxAdministrator/servletUnVetoed?id="+establishment,"JUKEBOX USERS VETOED",opciones);
}
        